(ns com.shadybrooksoftware.hunt-the-wumpus.events
  (:require
    [re-frame.core :refer [reg-event-fx reg-event-db reg-fx]]
    [com.shadybrooksoftware.hunt-the-wumpus.utils :as utils]
    [com.shadybrooksoftware.hunt-the-wumpus.game :as g]))

(reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    {:db {}}))

(reg-event-fx
  ::route-dispatch
  (fn [{:keys [db]} [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home {:dispatch [::set-active-panel :home-panel]}
        :game-board {:dispatch [::set-active-panel :game-board]}
        {:dispatch [::set-active-panel :invalid-route]}))))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(reg-event-fx
  ::new-game
  (fn [{:keys [db]} _]
    (let [game (g/new)]
      {:db        (-> db
                      (assoc :game game)
                      (dissoc :action))
       :set-route {:url (str "/game/" (:id game))}})))

(reg-event-db
  ::action
  (fn [db [_ action]]
    (assoc db :action action)))

(reg-event-fx
  ::perform-action-in-room
  (fn [{:keys [db]} [_ room]]
    (let [event (case (:action db)
                  :shoot [::shoot room]
                  :move [::move room])]
      {:fx [[:dispatch event]]})))

(reg-event-db
  ::shoot
  (fn [db [_ room]]
    (-> db
        (update :game g/shoot room)
        (update-in [:game :wumpus-dead?]
                   #(= room (get-in db [:game :wumpus-room])))
        (update :game #(if-not (:wumpus-dead? %)
                         (g/move-wumpus %)
                         %))
        (dissoc :action))))

(reg-event-db
  ::move
  (fn [db [_ room]]
    (let [game (g/move (:game db) room)]
      (-> db
          (dissoc :action)
          (assoc :game (if (g/in-bat-room? game)
                         (loop [game (g/bat-move game)]
                           (if (g/in-bat-room? game)
                             (recur (g/bat-move game))
                             game))
                         game))))))
