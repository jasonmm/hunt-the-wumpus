(ns com.shadybrooksoftware.hunt-the-wumpus.game)

(def cave-layout
  "Map of the cave layout associating rooms with their adjoining rooms."
  {1  [2, 5, 8]
   2  [1, 3, 10]
   3  [2, 4, 12]
   4  [3, 5, 14]
   5  [1, 4, 6]
   6  [5, 7, 15]
   7  [6, 8, 17]
   8  [1, 7, 9]
   9  [8, 10, 18]
   10 [2, 9, 11]
   11 [10, 12, 19]
   12 [3, 11, 13]
   13 [12, 14, 20]
   14 [4, 13, 15]
   15 [6, 14, 16]
   16 [15, 17, 20]
   17 [7, 15, 18]
   18 [9, 17, 19]
   19 [11, 18, 20]
   20 [13, 16, 19]})

(defrecord GameState
  [id human-room wumpus-room bat-rooms pit-rooms arrows events wumpus-dead?])

(defn new
  "Return a `GameState` record for the start of a new game."
  []
  (let [rooms     (shuffle (keys cave-layout))
        room-data {:human-room  (first rooms)
                   :wumpus-room (second rooms)
                   :bat-rooms   (take 2 (drop 2 rooms))
                   :pit-rooms   (take 2 (drop 4 rooms))}
        #_#_room-data {:human-room  9
                       :wumpus-room 10
                       :bat-rooms   [8 15]
                       :pit-rooms   [18 2]}]
    (map->GameState (merge room-data
                           {:id     (random-uuid)
                            :arrows 5
                            :events [(assoc room-data
                                       :event :game-created)]}))))

(defn adjoining-rooms
  [room-number]
  (get cave-layout room-number))

(defn shoot
  "Return the new game state after shooting into `room`."
  [game room]
  (-> game
      (update :arrows dec)
      (update :events conj {:event   :shoot
                            :missed? (not= room (:wumpus-room game))
                            :room    room})))

(defn move
  "Return the new game state after moving into `room`."
  [game room]
  (-> game
      (assoc :human-room room)
      (update :events conj {:event :move
                            :room  room})))

(defn in-bat-room?
  [game]
  (contains? (-> game :bat-rooms set) (:human-room game)))

(defn bat-move
  [game]
  (let [new-room (rand-nth (keys cave-layout))]
    (-> game
        (assoc :human-room new-room)
        (update :events conj {:event :bat-move
                              :room  new-room}))))

(defn move-wumpus
  [game]
  (let [rooms    (adjoining-rooms (:wumpus-room game))
        old-room (:wumpus-room game)
        new-room (rand-nth rooms)]
    (-> game
        (assoc :wumpus-room new-room)
        (update :events conj {:event    :wumpus-move
                              :old-room old-room
                              :room     new-room}))))
