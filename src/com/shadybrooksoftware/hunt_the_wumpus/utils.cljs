(ns com.shadybrooksoftware.hunt-the-wumpus.utils
  (:require
    [reagent.core]))

(def <sub (comp deref re-frame.core/subscribe))

(defn set-hash!
  [new-hash]
  (set! (.-hash js/window.location) new-hash))

(defn target-value
  "A helper function for getting the event target's value. Used primarily to
  minimize the number of IDE warnings showing in the code."
  [event]
  (-> event .-target .-value))

