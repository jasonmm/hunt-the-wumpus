(ns com.shadybrooksoftware.hunt-the-wumpus.views
  (:require
    [re-frame.core :as re]
    [goog.string :as gstring]
    [com.shadybrooksoftware.hunt-the-wumpus.routes :as routes]
    [com.shadybrooksoftware.hunt-the-wumpus.subs :as subs]
    [com.shadybrooksoftware.hunt-the-wumpus.events :as events]
    [com.shadybrooksoftware.hunt-the-wumpus.game :as g]
    [com.shadybrooksoftware.hunt-the-wumpus.utils :refer [<sub]]
    [clojure.string :as str]))

(defn- container
  [body]
  [:div.flex.flex-col.justify-center.max-w-md.m-6.pb-10.mx-auto body])

(defn- page-title
  [title]
  [:h1.text-5xl.mb-5 title])

(defn- primary-button
  [opts label]
  [:div.flex.justify-center.p-3
   [:button.py-3.px-6.bg-sky-500.text-black.rounded-xl.hover:bg-sky-900.hover:text-white.duration-300
    opts
    label]])

(defn- home-link
  []
  [:a.text-sky-500
   {:href (routes/url-for :home)}
   [:span
    (gstring/unescapeEntities "&larr; ")
    [:span.hover:underline "Home"]]])

(defn- inline-link
  [on-click link-text]
  [:span.text-sky-500.cursor-pointer.hover:underline
   {:on-click on-click}
   link-text])

(defn- action->text
  [action-kw]
  (str/capitalize (name action-kw)))

(defn- room-choice
  [action]
  (when action
    [:div
     (str (action->text action) " into which room? ")
     (for [room (<sub [::subs/adjoining-rooms])]
       ^{:key room}
       [:span.px-2
        [inline-link
         #(re/dispatch [::events/perform-action-in-room room])
         room]])]))

(defn- action-option
  [action selected-action]
  (if selected-action
    [:span
     {:class (if (= action selected-action) "font-bold")}
     (action->text action)]
    [inline-link
     #(re/dispatch [::events/action action])
     (action->text action)]))


(defn- dangers []
  [:<>
   (when (<sub [::subs/pit-nearby?])
     [:div.italic (gstring/unescapeEntities "&rarr;") " You feel a draft!"])
   (when (<sub [::subs/bats-nearby?])
     [:div (gstring/unescapeEntities "&rarr;") " You hear bats!"])
   (when (<sub [::subs/wumpus-nearby?])
     [:div.font-bold (gstring/unescapeEntities "&rarr;") " There is a stench nearby!!"])])

(defn- game-over
  [reason]
  [:<>
   (case reason
     :wumpus [:div "You have been eaten by the wumpus!!"]
     :pit [:div "You fell in a bottomless pit!"]
     :no-arrows [:div "You have run out of arrows. The wumpus attacks and kills you!"]
     :wumpus-dead [:div.font-bold.text-emerald-600 "You have killed the wumpus!"])
   [:div.p-3.rounded-xl.my-3.text-center.space-y-3
    {:class (if (= :wumpus-dead reason) "bg-emerald-600" "bg-slate-800")}
    [:h1.text-2xl.mx-auto.text-white.font-bold "Game Over!"]
    [:div.flex.justify-center.p-3
     [:button.py-3.px-6.bg-slate-200.text-black.rounded-xl.hover:bg-sky-500.hover:text-white.duration-300
      {:on-click #(re/dispatch [::events/new-game])}
      "New Game"]]]])

(defn- game-current
  [game]
  (let [selected-action  (<sub [::subs/action])
        game-over-reason (<sub [::subs/game-over-reason])]
    [:div
     [:div "You are in room " (:human-room game)]
     [:div "Tunnels lead to rooms " (->> game
                                         :human-room
                                         g/adjoining-rooms
                                         (str/join ", "))]
     (if game-over-reason
       [game-over game-over-reason]
       [:<>
        [dangers]
        [:div
         "Do you want to "
         [action-option :shoot selected-action]
         " or "
         [action-option :move selected-action]]
        (when (= :shoot selected-action)
          [:div "You have " (:arrows game) " arrows remaining."])
        [room-choice selected-action]])]))

(defn history-item
  [event]
  [:div.border-b.border-slate-200.p-3
   {:key (:key event)}
   (case (:event event)
     :game-created [:span "Game created; you are in room " (:human-room event)]
     :move [:span "Moved to room " (:room event)]
     :shoot [:span "Shot an arrow into room " (:room event) "."
             (when (:missed? event) [:span " Missed."])]
     :wumpus-move [:span "The wumpus moved."]
     :bat-move [:span "Bats have carried you to room " (:room event)])])

(defn game-history
  [game]
  (let [events (->> game
                    :events
                    (map-indexed (fn [i e] (assoc e :key i)))
                    reverse)]
    [:<>
     [:hr.my-3]
     [:h1.text-lg.font-bold "Game History"]
     (for [event events]
       (history-item event))]))

(defn game-not-found
  []
  [:div.bg-red-800.p-6.rounded-xl.mt-3.text-center
   [:h1.text-2xl.mx-auto.text-white.font-bold "Invalid Game"]])

(defn home-panel []
  [container
   [:<>
    [page-title "Hunt A Wumpus!"]
    [:p.my-3 "The Wumpus lives in a cave of 20 rooms. Each room has 3 tunnels leading to other rooms."]
    [:p.my-3 "There are two rooms with pits, two rooms with bats, and one room with the Wumpus. If you enter a room with a pit you will fall to your death. If you enter a room with a bat then the bat will pick you up and carry you to a random room (which my be troublesome). If you enter a room with the Wumpus then the Wumpus will eat you."]
    [:p.my-3 "Each turn you may move or shoot an arrow into an adjoining room. If you hit the Wumpus with an arrow you win."]
    [:hr]
    [primary-button
     {:on-click #(re/dispatch [::events/new-game])}
     "New Game"]]])

(defn game-board []
  (let [game (<sub [::subs/game])]
    [container
     [:<>
      [page-title "Hunt A Wumpus!"]
      [:p.my-3 [home-link]]
      [:hr]
      (if-not game
        [game-not-found]
        [:<>
         [game-current game]
         [game-history game]])]]))

(defn invalid-route []
  [:div.flex.flex-col.justify-center.max-w-md.m-6.mx-auto
   [:h1.text-5xl.mb-5 "Invalid Route"]])

(defn main-panel
  []
  (case (<sub [::subs/active-panel])
    :home-panel [home-panel]
    :game-board [game-board]
    :invalid-route [invalid-route]
    [:div "Invalid panel"]))
