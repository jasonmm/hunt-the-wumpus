(ns com.shadybrooksoftware.hunt-the-wumpus.subs
  (:require
    [re-frame.core :refer [reg-sub]]
    [com.shadybrooksoftware.hunt-the-wumpus.game :as g]
    [clojure.set :as set]))

(reg-sub ::active-panel (fn [db _] (:active-panel db)))
(reg-sub ::game (fn [db _] (:game db)))
(reg-sub ::action (fn [db _] (:action db)))

(reg-sub
  ::arrows
  :<- [::game]
  (fn [game]
    (:arrows game)))

(reg-sub
  ::adjoining-rooms
  :<- [::game]
  (fn [game]
    (g/adjoining-rooms (:human-room game))))

(reg-sub
  ::pit-nearby?
  :<- [::game]
  :<- [::adjoining-rooms]
  (fn [[game rooms-next-to-human]]
    (seq (set/intersection (set rooms-next-to-human)
                           (-> game :pit-rooms set)))))

(reg-sub
  ::bats-nearby?
  :<- [::game]
  :<- [::adjoining-rooms]
  (fn [[game rooms-next-to-human]]
    (seq (set/intersection (set rooms-next-to-human)
                           (-> game :bat-rooms set)))))

(reg-sub
  ::wumpus-nearby?
  :<- [::game]
  :<- [::adjoining-rooms]
  (fn [[game rooms-next-to-human]]
    (contains? (set rooms-next-to-human) (:wumpus-room game))))

(reg-sub
  ::in-pit-room?
  :<- [::game]
  (fn [game]
    (contains? (-> game :pit-rooms set) (:human-room game))))

(reg-sub
  ::in-wumpus-room?
  :<- [::game]
  (fn [game]
    (= (:wumpus-room game)
       (:human-room game))))

(reg-sub
  ::wumpus-dead?
  :<- [::game]
  (fn [game]
    (:wumpus-dead? game)))

(reg-sub
  ::game-over?
  :<- [::in-pit-room?]
  :<- [::in-wumpus-room?]
  :<- [::wumpus-dead?]
  :<- [::arrows]
  (fn [[pit? wumpus? wumpus-dead? arrows-remaining]]
    (or pit? wumpus? (zero? arrows-remaining) wumpus-dead?)))

(reg-sub
  ::game-over-reason
  :<- [::in-pit-room?]
  :<- [::in-wumpus-room?]
  :<- [::wumpus-dead?]
  :<- [::arrows]
  (fn [[pit? wumpus? wumpus-dead? arrows-remaining]]
    (cond
      wumpus-dead? :wumpus-dead
      (zero? arrows-remaining) :no-arrows
      pit? :pit
      wumpus? :wumpus
      :else nil)))

